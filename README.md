# Overview

This project comprises Model/Controllers. Sample mysql database has been attached **db.sql**

In order to run the project locally simply update **config.json** parameters for the database, and then run *chalice local* from the chalice project root direcotry.

# Pre-requisits

From terminal/command-prompt install the following packages:
- pip install boto3
- pip install botocore
- pip install pymysql
- pip install requests
- pip install numpy

# More Features
- Auto Deployment script with sample data filled. NOTE: Change those dummy parameters before using
- Separate configuration for different chalice environments
- Basic access features such as login/logout/index/details/insert/update/delete

# Sample API Calls (Examples)

**localhost:8000**
Request Method: GET

**localhost:8000/site/signup**
Content-Type: application/json
Request Method: POST
{
	"email": "mubashir4ali@yahoo.com",
	"password": "password1234",
	"first_name":"Mubashir1",
	"last_name": "Ali",
	"status": "active"
}

**localhost:8000/site/login**
Content Type: application/json
Request Method: POST
{
	"email": "mubashir4ali@yahoo.com",
	"password": "password1234"
}

Response will contain the access token.
{
    "id": 1,
    "first_name": "Mubashir",
    "last_name": "Ali",
    "email": "mubashir4ali@yahoo.com",
    "status": "active",
    "token": "op9uxf98c1d6cll9ewmqw33rqxkzpzdp5cklen2rumv3be76ctnqe4sfo8u588svd7c69uuogo42qx7kdk84knfnv4r1n9grv6tm"
}

**localhost:8000/users**
Request Method: GET
Header: Authorization: RETURNED TOKEN AFTER LOGIN (op9uxf98c1d6cll9ewmqw33rqxkzpzdp5cklen2rumv3be76ctnqe4sfo8u588svd7c69uuogo42qx7kdk84knfnv4r1n9grv6tm)

**localhost:8000/users/1**
Request Method: GET
Header: Authorization: RETURNED TOKEN AFTER LOGIN (op9uxf98c1d6cll9ewmqw33rqxkzpzdp5cklen2rumv3be76ctnqe4sfo8u588svd7c69uuogo42qx7kdk84knfnv4r1n9grv6tm)

**localhost:8000/users/2**
Request Method: DELETE
Header: Authorization: RETURNED TOKEN AFTER LOGIN (op9uxf98c1d6cll9ewmqw33rqxkzpzdp5cklen2rumv3be76ctnqe4sfo8u588svd7c69uuogo42qx7kdk84knfnv4r1n9grv6tm)

**localhost:8000/users/1**
Request Method: PUT/PATCH
Header: Authorization: RETURNED TOKEN AFTER LOGIN (op9uxf98c1d6cll9ewmqw33rqxkzpzdp5cklen2rumv3be76ctnqe4sfo8u588svd7c69uuogo42qx7kdk84knfnv4r1n9grv6tm)
{"first_name":"Mubashir Updated"}

**localhost:8000/public**
Request Method: GET
Another public controller
without Access Token

**localhost:8000/another**
Request Method: GET
Header: Authorization: RETURNED TOKEN AFTER LOGIN (op9uxf98c1d6cll9ewmqw33rqxkzpzdp5cklen2rumv3be76ctnqe4sfo8u588svd7c69uuogo42qx7kdk84knfnv4r1n9grv6tm)


> NOTE: Open to redicules, as this is just start of the project.
