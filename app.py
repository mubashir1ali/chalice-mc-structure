from chalice import (CORSConfig, Chalice, Response, BadRequestError,
                     UnauthorizedError, ForbiddenError,
                     NotFoundError, ChaliceViewError, AuthResponse)
                     
import os
import logging
import json
import requests
import pymysql
import boto3
import numpy
import cgi
import json
from io import BytesIO

from datetime import date
from chalicelib.models.users import Users
from chalicelib.utility.functions import (respond,ret_error,get_json_data,validate_uuid4)

logger = logging.getLogger()
logger.setLevel(logging.INFO)

app = Chalice(app_name='chalice-mc-structure')

app.debug = True
cors_config = True

@app.route('/')
def index():
    return {'Project Name': 'Chalice Model Controller Home Page'}

### Create Custom Authorizer Function
def validate_auth_token():

    appData = {}

    # Pass parameter in the header
    # Parameter name is: 'Authorization'
    
    app_header = app.current_request.headers
    token = app_header.get('Authorization')

    objU = Users()
    if objU.validate_token(token):        
        appData = {}
        appData['user'] = objU.validate_token(token,True)

        objU.update_token_time(token)

        return appData

    return False

@app.route('/{controller}', methods=['GET','POST','PUT','DELETE','PATCH'], cors=cors_config)
def mvc_controller(controller):
    return mvc_core(controller)

@app.route('/{controller}/{action}', methods=['GET','POST','PUT','DELETE','PATCH'], cors=cors_config)
def mvc_controller_action(controller, action):
    return mvc_core(controller, action)

@app.route('/{controller}/{action}/{id}', methods=['GET','POST','PUT','DELETE','PATCH'], cors=cors_config)
def mvc_controller_action_id(controller, action, id):
    return mvc_core(controller, action, id)

def mvc_core(controller=None, action=None, id=None):
    
    #auth_token = app.current_request.headers.get('Authorization')
    #if not my_auth(auth_token):
    #    return ret_error("Not Authorised",403)
    

    # Controller Resources
    #/controller	GET         Listing
    #/controller/id	GET         Details --- it is id or uuid
    #/controller	POST        Insert Operation
    #/controller/id	PUT/PATCH   Update Operation
    #/controller/id	DELETE      Delete Operation
    
    if not controller:
        return ret_error("Invalid Controller")
    
    try:
        import importlib
        
        app_dict = app.current_request.to_dict()
        request_method = app_dict['method']

        module = importlib.import_module('chalicelib.controllers.'+controller.replace('-', '_')+'_controller')

        controller_class = getattr(module, controller.replace('-', '').replace('_', '').title()+'Controller')
        controller_instance = controller_class(app=app)
        
        if controller_instance.auth_required:
            # perform the login function, if return False, then send Access Denied Message
            # User.Login
            controller_instance.user_data = validate_auth_token()
            if not controller_instance.user_data:
                return ret_error("Unauthorized access is not allowed", 401)
        
        act = action
        if not action:
            act = "index"
        else:
            try:
                id = int(action)
                act = "index"
            except ValueError:
                if validate_uuid4(action):  #c9cc91b5-cb42-4de1-abe3-e57e234f41c1
                    act = "index"
                    id = action

        # Calling Resources
        if request_method == 'GET' and act == 'index':
            if not id:
                result = controller_instance.index()
            else:
                result = controller_instance.details()
        elif request_method == 'POST' and act == 'index':
            result = controller_instance.insert()
        elif request_method in ['PUT','PATCH'] and act == 'index' and id:
            result = controller_instance.update()
        elif request_method == 'DELETE' and act == 'index' and id:
            result = controller_instance.delete()
        else:
            method_to_call = getattr(controller_instance, act.replace('-', '_') )
            result = method_to_call()

    except Exception as exp:
        return ret_error(format(exp))
    
    return result
