from .base_controller import BaseController
from ..utility.functions import (respond, ret_error,get_json_data)

class AnotherController(BaseController):

    def __init__(self, **kwargs):
        super().__init__(app=kwargs['app'])
    
    def index(self):
        return {'action':'another listing'};

    def details(self):
        return {"action":"another details"}
    
    def insert(self):
        return {"action":"another insert"}
    
    def update(self):
        return {"action":"another update"}
    
    def delete(self):
        return {"action":"another delete"}
    
    def custom_function(self):
        return {"action": "another custom function"}
