import warnings
warnings.filterwarnings("error")
class BaseController:
    def __init__(self, **kwargs):
        self.app = kwargs['app']
        self.app_curr_req   = self.app.current_request
        self.app_req_data   = self.app.current_request.to_dict() 
        self.app_req_header = self.app.current_request.headers 
        if self.app_req_data['method'] == 'GET':
            self.app_json_body  = {}
        else:
            self.app_json_body  = self.app.current_request.json_body        
        self.app_context    = self.app.current_request.context
        
        self.auth_required = True
        
        self.user_data = {}