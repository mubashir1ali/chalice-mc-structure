from .base_controller import BaseController
from ..utility.functions import (respond, ret_error,get_json_data)

class PublicController(BaseController):

    def __init__(self, **kwargs):
        super().__init__(app=kwargs['app'])
        self.auth_required = False
    
    def index(self):
        return {'action':'public listing'};

    def details(self):
        return {"action":"public details"}
    
    def insert(self):
        return {"action":"public insert"}
    
    def update(self):
        return {"action":"public update"}
    
    def delete(self):
        return {"action":"public delete"}
    
    def custom_function(self):
        return {"action": "another custom function"}
