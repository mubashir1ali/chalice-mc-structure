from .base_controller import BaseController
from ..utility.functions import (respond, ret_error,get_json_data)

from ..models.users import Users

class SiteController(BaseController):

    def __init__(self, **kwargs):
        super().__init__(app=kwargs['app'])
        
        ### On Site Controller No Authentication Required
        self.auth_required = False
        
        self.objU = Users()
    
    def index(self):
        # It might move to users_controller
        if not self.app_req_data['query_params']:
            self.app_req_data['query_params'] = {}

        return self.objV.listing(True,data=self.app_req_data)
    
    def details(self):
        # It might move to users_controller
        uuid = self.app_req_data['uri_params']['action'] #Here action behaves as id or uuid
        return self.objV.details(uuid)
    
    def insert(self):
        # It might move to users_controller
        request_dataei = {'query_params':""}

        try:
            request_dataei['query_params'] = self.app_json_body
        except Exception as jde:
            return ret_error(format(jde))

        return self.objV.insert(http_response=True,data=request_dataei['query_params'])
    
    def update(self):
        # It might move to users_controller
        uuid = self.app_req_data['uri_params']['action'] #Here action behaves as id or uuid
        jdata=get_json_data(self.app_curr_req)
        return self.objV.update(data=jdata,uuid=uuid)
    
    def delete(self):
        # It might move to users_controller
        uuid = self.app_req_data['uri_params']['action'] #Here action behaves as id or uuid
        return self.objV.delete(uuid)
    
    ######## Custom Functions #########
    
    def login(self):
        json_data = False

        try:
            #json_data = app.current_request.json_body
            json_data = self.app_json_body
        except Exception as jde:
            return ret_error(format(jde))

        em = ''
        pw = ''

        if json_data:
            if 'email' in json_data:
                em = json_data['email']

            if 'password' in json_data:
                pw = json_data['password']

        resp = self.objU.login(email=em,password=pw)

        return resp

    def logout(self):
        if 'authorization' in self.app_req_header:
            token = self.app_req_header['authorization']
            return self.objU.logout(token)

        return ret_error("Invalid Token")

    def signup(self):
        
        jdata=get_json_data(self.app_curr_req)

        user_data = {}
        user_data['first_name'] = jdata['first_name']
        user_data['last_name'] = jdata['last_name']
        user_data['email'] = jdata['email']
        user_data['password'] = jdata['password']
        user_data['status'] = jdata['status']

        resp = self.objU.signup(data=user_data,insert_level="signup")
        return resp

    def complete_signup(self):
        request_data = self.app_req_data

        try:
            token = request_data['query_params']['token']
        except Exception as e:
            print(e)
            return ret_error("Token Missing")

        return self.objU.complete_signup(reset_token=token)

    def resend_signup_token(self):
        request_data = self.app_req_data
        resp = self.objU.resend_signup_token(data=request_data)
        return resp

    def invite(self):
        return {'User': 'Invite'}

    def forgot_password(self):
        objU = Users()
        return objU.forgotPassword(data=self.app_json_body)

    def reset_password(self):
        objU = Users()
        return objU.resetPassword(data=self.app_json_body)