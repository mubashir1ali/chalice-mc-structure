from .base_controller import BaseController
from ..utility.functions import (respond, ret_error,get_json_data)

from ..models.users import Users

class UsersController(BaseController):

    def __init__(self, **kwargs):
        super().__init__(app=kwargs['app'])
        self.objU = Users()
    
    def index(self):
        
        if not self.app_req_data['query_params']:
            self.app_req_data['query_params'] = {}

        return self.objU.listing(data=self.app_req_data)
    
    def details(self):
        uuid = self.app_req_data['uri_params']['action'] #Here action behaves as id or uuid
        return self.objU.details(uuid)
    
    def insert(self):
        user_data = self.app_json_body
        if not user_data:
            user_data = {}
        
        return self.objU.signup(data=user_data,insert_level="user")
    
    def update(self):
        id = self.app_req_data['uri_params']['action'] #Here action behaves as id or uuid
        jdata=get_json_data(self.app_curr_req)
        return self.objU.update(data=jdata,id=id)
    
    def delete(self):
        id = self.app_req_data['uri_params']['action'] #Here action behaves as id or uuid
        return self.objU.delete(id)
    
    def change_password(self):
        return self.objU.changePassword(user_id=self.user_data['user']['id'],data=self.app_json_body)

    def forgot_password(self):
        return self.objU.forgotPassword(data=self.app_json_body)

    def reset_password(self):
        return self.objU.resetPassword(data=self.app_json_body)