from ..utility.functions import (list_to_json,set_to_json,sha256,respond, validate_email, get_currentdatetime, ret_error)
from ..utility.simplemysql import SimpleMysql
from .base_model import BaseModel

import uuid
import sys
import json
import string
import random
import uuid
import boto3
import warnings
import datetime
import os
warnings.filterwarnings("error")

class Users(BaseModel):
    def __init__(self, **kwargs):
        self.objdb = SimpleMysql()        
        self.table_name = "users"
        
        if 'first_name' in kwargs:
            self.first_name = kwargs['first_name']
        
        if 'last_name' in kwargs:
            self.last_name = kwargs['last_name']
        
        if 'email' in kwargs:
            self.email = kwargs['email']
        
        if 'password' in kwargs:
            self.password = sha256(kwargs['password'])
        
        if 'status' in kwargs:
            self.status = kwargs['status']
        
    def __str__(self):
        return str(self.__class__) + ": " + str(self.__dict__)
    
    def login(self, **params):

        if not params['email']:
            return ret_error("Email address is missing")

        if not params['password']:
            return ret_error("Password is missing")
        
        email = params['email']
        #password = sha256(params['password']) 
        password = params['password']   #it is for the testing only
        
        user = self.objdb.getOne(self.table_name,
                ["id", "first_name","last_name","email","status"],
                ("`email` = %s AND `password` = %s", (email, password))
        )
        if not user:
            return ret_error("Invalid Email/password")
        else:

            if user['status'] != 'active':
                return ret_error("Account is not active")
            else:
                try:
                    
                    stoken = self.generate_token()
                    user['token'] = stoken

                    self.objdb.update(self.table_name,
                        {"token": stoken},
                        ("`id` = %s ", [user['id']])
                    )

                    self.update_token_time(stoken)

                except Exception as e:
                    return respond({"message":format(e)},500)
                return user
            
    
    def logout(self, token):

        try:
            self.objdb.update(self.table_name,
                {"token": None},
                ("`token` = %s ", [token])
            )
            return respond({"message":"logged out successfully"},200)
        except Exception as e:
            return respond({"error":format(e)},500)
    
    def listing(self, **prms):

        params = prms['data']
        strWhere = '\'1\'=%s'
        aryWhere = []
        aryWhere.append(1)

        page = 0
        
        if params:

            if 'first_name' in params.keys():
                strWhere += " AND first_name LIKE %s "
                aryWhere.append("%"+params['first_name']+"%")

            if 'last_name' in params.keys():
                strWhere += " AND last_name LIKE %s "
                aryWhere.append("%"+params['last_name']+"%")

            if 'email' in params.keys():
                strWhere += " AND email LIKE %s "
                aryWhere.append(params['email'])

            if 'status' in params.keys():
                strWhere += " AND status LIKE %s "
                aryWhere.append(params['status'])

            if 'keyword' in params.keys():
                strWhere += " AND ( "
                strWhere += " first_name LIKE %s OR "
                strWhere += " last_name LIKE %s OR "
                strWhere += " email LIKE %s "
                strWhere += " ) "
                aryWhere.append("%"+params['keyword']+"%")
                aryWhere.append("%"+params['keyword']+"%")
                aryWhere.append("%"+params['keyword']+"%")

            if 'page' in params.keys():
                try:
                    page = int(params['page'])
                except Exception as e:
                    return ret_error(format(e))
                page = page - 1
                if 0>page:
                    page = 0
                page = int(page)*int(os.environ['ROWS_PER_PAGE'])

        return respond(self.objdb.getAll(self.table_name,
                    fields=["id","first_name","last_name","email","status"],
                    where=(strWhere, aryWhere),
                    limit=(page,os.environ['ROWS_PER_PAGE'])
                ), 200)
                
    def details(self, id):
        user = self.objdb.getOne(self.table_name,
            ["*"],
            ("`id` = %s", (id))
        )
        
        if not user:
            return ret_error("No Record found")
        
        return respond(user,200)

    def signup(self, **params_passed):
        params = params_passed['data']
        
        if 'email' not in params.keys():
            return ret_error("Email address is missing")
           
        if 'first_name' not in params.keys():
            if insert_level == 'signup':
                ObjE.delete(params['entity_id'])
            return ret_error("First name is missing")
            
        if 'last_name' not in params.keys():
            if insert_level == 'signup':
                ObjE.delete(params['entity_id'])
            return ret_error("Last name is missing")
            
        if 'password' in params.keys():
            params['password'] = params['password']
            #params['password'] = sha256(params['password'])
            
        #params['uuid'] = str(uuid.uuid4())
        #params['created_at'] = get_currentdatetime()
        #params['updated_at'] = get_currentdatetime()       
        
        #params['reset_token'] = self.generate_token()      #Commented now for creating manual account

        
        try:
            res = self.objdb.insert(self.table_name, params)
            return respond("User Inserted successfully",200)
        except Exception as e:
            return ret_error(format(e))
            
    def complete_signup(self, **params):
        
        token_exist = self.objdb.getOne(self.table_name,
            ["email"],
            ("`reset_token` = %s ", (params['reset_token']))
        )

        if not token_exist:
            return ret_error("Invalid Token")
        
        try:
            user = self.objdb.getOne(self.table_name,
                ["status","id"],
                ("`reset_token` = %s ", (params['reset_token']))
            )
            
            res = self.objdb.update(
                    self.table_name,
                    {'reset_token':None,'token':params['reset_token'],'status': 'active', 'updated_at': get_currentdatetime()},
                    ("`reset_token` = %s ", [params['reset_token']])
                )

            if user['status'] =="online-act-and-password" and res:
                return respond({"success":"reset_password","user_id":user['id']},200)
                
        except Exception as e:
            return ret_error(format(e))

        if not res:
            return ret_error("Invalid Token")

        return respond({"success":"activated"},200)
        
    def resend_signup_token(self, **request_data):
        if not request_data['data']['query_params']:
            return ret_error("Invalid data passed")
        
        params = request_data['data']['query_params']
        
        if 'email' not in params.keys():
            return ret_error("Email address is missing")

        if(validate_email(params['email']) == False):
            return respond({"error":"Invalid Email Address"},500)
        
        user = self.objdb.getOne(self.table_name,
            ["email","reset_token","first_name"],
            ("`email` = %s AND status IS NULL ", (params['email']))
        )
        
        if not user:
            return ret_error("Email not found")
        
        from chalicelib.utility.mailgun import Mailgun
        mailgun = Mailgun()
        try:
            email_html = "Hi "+user['first_name']+",<br /><br />Welcome to the Streamline Equals. <br /><br />Here is your reset token<br /><a href='"+os.environ['SITE_DOMAIN']+"/#/resend?token="+user['reset_token']+"'>Click Here</a> to complete the Signup process.<br /><br />Streamline Equals"

            resm = mailgun.sendmail(event={"email_to":params['email'], 
                                        "subject":"Streamline Equals - Resending - Signup Token",
                                        "html":email_html})
            return respond({"message":params},200)
        except Exception as e:
            return ret_error(format(e))


    def changePassword(self, user_id, **params):

        if not params['data']:
            params['data'] = {}
        
        if 'old' not in params['data'].keys():
            return ret_error("Old Password is missing")
        if 'new' not in params['data'].keys():
            return ret_error("New Password is missing")
        if 'confirm' not in params['data'].keys():
            return ret_error("Confirm Password is missing")
        
        old = sha256(params['data']['old'])
        new = sha256(params['data']['new'])
        confirm = sha256(params['data']['confirm'])

        if new != confirm:
            return ret_error("Confirmed password does not match")
        
        user = self.objdb.getOne(self.table_name,["id"],("`id` = %s AND `password` = %s", (user_id, old)))
        
        if not user:
            return ret_error("Old password does not match")

        self.objdb.update(self.table_name,
            {"password": new},
            ("`id` = %s ", [user['id']])
        )
        return True

    def forgotPassword(self,**params):
        
        if not params['data']:
            params['data'] = {}
        
        if 'email' not in params['data'].keys():
            return ret_error("Email address is missing")
        
        user = self.objdb.getOne(self.table_name,["id","token","email","first_name","last_name"],("`email` = %s ", (params['data']['email'])))
        if not user:
            return ret_error("Email Address Not Found")
        
        from chalicelib.utility.mailgun import Mailgun

        mailgun = Mailgun()
        event = {}
        event['email_to'] = params['data']['email']
        event['subject'] = "Streamline Equals - Password Reset Request"
        event['html'] = "Hi "+user['first_name']+" "+user['last_name']+",<br /><br />Please <a href='"+os.environ['SITE_DOMAIN']+"/#/reset?token="+user['token']+"~~~"+str(user['id'])+"'>Click Here</a> to reset your password<br /><br />Streamline Equals"
        return mailgun.sendmail(event)

    def resetPassword(self,**params):
        
        if not params['data']:
            params['data'] = {}
        
        if 'token' not in params['data'].keys():
            return ret_error("Token is missing")
        
        if 'password' not in params['data'].keys():
            return ret_error("Password is missing")
        
        if 'confirm' not in params['data'].keys():
            return ret_error("Confirm Password is missing")
        
        aryToken = params['data']['token'].split('~~~')
        
        user = self.objdb.getOne(self.table_name,["id"],("`token` = %s AND `id` = %s ", (aryToken[0],aryToken[1])))
        
        if not user:
            return ret_error("Invalid Token")
        
        return respond(
            self.objdb.update(self.table_name,
                {"password": sha256(params['data']['password'])},
                ("`id` = %s ", [user['id']])
            ),200)
        
    
    def generate_token(self, size=100, chars=string.ascii_lowercase + string.digits):
        generated_token = ''.join(random.choice(chars) for _ in range(size))
        user_has_token = self.objdb.getOne(self.table_name,
                ["token"],
                ("`token` = %s", (generated_token))
        )
        if user_has_token:
            self.generate_token()
        return generated_token

    def validate_token(self,token,get_data=False):
        
        if not token:
            return False

        user = self.objdb.getOne(self.table_name,
                ["token"],
                ("`token` = %s  AND token_last_activity > NOW() - INTERVAL "+os.environ['TOKEN_TIMEOUT']+" HOUR", (token))
        )

        if not user:
            return False
        else:
            if get_data == True:
                user = self.objdb.getOne(self.table_name,
                        ["id","first_name","last_name","email"],
                        ("`token` = %s ", (token))
                )
                return user
            return True
    
    def update_token_time(self, token):
        self.objdb.update(self.table_name,
            {"token_last_activity": datetime.datetime.now()},
            ("`token` = %s ", [token])
        )

        
    def update(self, **params):

        params['data']['updated_at'] = get_currentdatetime()
        
        if 'password' in params['data'].keys():
            params['data']['password'] = params['data']['password']
            #params['data']['password'] = sha256(params['data']['password'])
        
        try:
            res = self.objdb.update(self.table_name,params['data'],("`id` = %s ", [params['id']]))
        except Exception as e:
            return ret_error(format(e))

        if not res:
            return ret_error("No Record Found")
        
        return respond(res,200)
    
    def delete(self, id):
        
        try:
            res = self.objdb.delete(self.table_name, (" id = %s ", [id]))
        except Exception as e:
            return ret_error(format(e))

        if not res:
            return ret_error("No Record Found")
        
        return respond(res,200)
