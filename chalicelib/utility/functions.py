import hashlib
from chalice import (Response)
import json

def list_to_json(list):
    r_array = []
    for row in list:
        #tmp_row = {}
        #for key in row:
        #    tmp_row[key] = str(row[key])
        tmp_row = set_to_json(row)
        r_array.append(tmp_row)

    return r_array

def set_to_json(row):
    tmp_row = {}
    for key in row:
        tmp_row[key] = str(row[key])
    return tmp_row

def sha256(hashfood):

    b_string = hashfood.encode('utf-8')

    #hasing with hashlib
    hash_object = hashlib.sha256(b_string)
    hex_dig = hash_object.hexdigest()

    return hex_dig

def respond(bodymsg, statuscode, header=None):
    """creates function to handle sending out responds
    """
    if isinstance(bodymsg, dict):
        import json
        bodymsg = json.dumps(bodymsg)
        header = {'Content-Type': 'application/json'}

    if not header:
        return Response(body=bodymsg, status_code=statuscode)
    return Response(body=bodymsg, status_code=statuscode, headers=header)

def ret_error(msg, error_code=500):
    return respond({"error":msg},error_code)

def validate_token():
    token = app.current_request.headers.get('token')
    try:
        return User.validate_token(token)
    except Exception as error:
        raise UnauthorizedError(format(error))
    

'''
 Following functions will be used for Transcode Generation For SmartPay
'''
    
def increment_char(c):
    """
    Increment an uppercase character, returning 'A' if 'Z' is given
    """
    return chr(ord(c) + 1) if c != 'Z' else 'A'

def increment_str(s):
    s = s.upper()
    lpart = s.rstrip('Z')
    if not lpart:  # s contains only 'Z'
        new_s = 'A' * (len(s) + 1)
    else:
        num_replacements = len(s) - len(lpart)
        new_s = lpart[:-1] + increment_char(lpart[-1])
        new_s += 'A' * num_replacements
    return new_s

def validate_email(eml):
    import re
    match = re.match('^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$', eml)
    print(match)

    if not match:
        return False
    return True

def get_currentdatetime(**params):
    from time import gmtime, strftime
    return strftime("%Y-%m-%d %H:%M:%S", gmtime())

def get_json_data(req):
    import sys
    sys.tracebacklimit = 0
    try:
        return req.json_body
    except Exception as jde:
        raise TypeError(jde)
    

def validate_uuid4(uuid_string):
    from uuid import UUID
    #import uuid
    #uuid.uuid4()
    """
    Validate that a UUID string is in
    fact a valid uuid4.
    Happily, the uuid module does the actual
    checking for us.
    It is vital that the 'version' kwarg be passed
    to the UUID() call, otherwise any 32-character
    hex string is considered valid.
    """
    #print("Validating the Version")
    #print(UUID(uuid_string).version)
    try:
        #val = UUID(uuid_string, version=4)
        val = UUID(uuid_string, version=UUID(uuid_string).version)
    except ValueError:
        # If it's a value error, then the string 
        # is not a valid hex code for a UUID.
        return False

    # If the uuid_string is a valid hex code, 
    # but an invalid uuid4,
    # the UUID.__init__ will convert it to a 
    # valid uuid4. This is bad for validation purposes.

    #return val.hex == uuid_string
    return val.hex == uuid_string.replace('-', '')

def power_set(set_):
    subsets = [[]]
    for element in set_:
        for ind in range(len(subsets)):
            subsets.append(subsets[ind] + [element])
    return subsets

def _parse_form_data(current_request):
    body = current_request.raw_body

    content_type_header = current_request.headers['content-type']
    content_type, property_dict = cgi.parse_header(content_type_header)

    property_dict['boundary'] = bytes(property_dict['boundary'], "utf-8")
    form_data = cgi.parse_multipart(BytesIO(body), property_dict)
    return form_data

def _get_query_string():
    request_datau = app.current_request.to_dict()
    
    if not request_datau['query_params']:
        return ""
    else:
        from urllib.parse import urlencode
        return urlencode(request_datau['query_params'])
    
def _get_parts():
    rfile = BytesIO(app.current_request.raw_body)
    content_type = app.current_request.headers['content-type']
    print(content_type)
    return "Hello Content Type"
    _, parameters = cgi.parse_header(content_type)
    print(parameters)
    parameters['boundary'] = parameters['boundary'].encode('utf-8')
    parsed = cgi.parse_multipart(rfile, parameters)
    return parsed

# This function returns the levenshtein length, needs 
def levenshtein_length(str_value):
    
    kw_len = len(str_value)
    
    if kw_len < 6:
        lev_len = 2
    elif kw_len < 11:
        lev_len = 3
    elif kw_len < 16:
        lev_len = 4
    elif kw_len < 21:
        lev_len = 5
    else:
        lev_len = 6
        
    return lev_len