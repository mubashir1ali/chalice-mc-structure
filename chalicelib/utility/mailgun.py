import os
import json
import requests

class Mailgun():
    """Class for Mailgun functions
    """

    def __init__(self):
        """Assign concrete class to self
        """


    def sendmail(self, event):
        
        to_email = event['email_to']
        #to_email = "mubashira@sublimegroup.net"
        
        resp = requests.post(
            os.environ['MAILGUN_DOMAIN'],
            auth=("api", os.environ['MAILGUN_KEY']),
            data={"from": os.environ['EMAIL_FROM'],
                  "to": to_email,
                  "bcc": "mubashira@sublimegroup.net",
                  "subject": event['subject'],
                  "html": event['html']})
        if resp.status_code == 200:
            result = {"Status": "Success", "Message": "email send successfully", "Code": 200}
        else:
            result = {"Status": "Failed", "Message": resp.content, "Code": 500}
        print(result)
        return result