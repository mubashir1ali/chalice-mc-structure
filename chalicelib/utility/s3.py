import botocore
import boto3
import os
import json
import base64

class S3():
    s3_bucket_name = "sublime-serko-ocr"

    obj = boto3.resource('s3')

    def __init__(self):
        self.client = boto3.client('s3')
        self.resource = boto3.resource('s3')
        self.bucket_name = os.environ['S3_BUCKET_NAME']

    def download(self, file_name, store_path):
        try:
            print("in s3 download function: " + S3.s3_bucket_name + ", filename: " + file_name + ", storepath: " + store_path)
            S3.obj.Bucket(S3.s3_bucket_name).download_file(file_name,
                                                                     store_path)
            # print("s3 download function done")
        except botocore.exceptions.ClientError as e:
            # print("in s3 download exception occurred:" + e.response['Error']['Code'])
            if e.response['Error']['Code'] == "404":
                print("failed to download file.")
            else:
                raise

    def upload(self, file_name, store_path):
        try:
            S3.obj.Bucket(S3.s3_bucket_name).upload_file(file_name,
                                                                   store_path)
        except Exception as e:
            print("failed to upload file")
            raise e

    def geturl(self, key, filename):
        print("geturl function, using bucket : " + S3.s3_bucket_name)
        s3 = boto3.client('s3')
        r_array = {}
        try:
            url = s3.generate_presigned_url(
                ClientMethod='get_object',
                ExpiresIn=600,
                Params={
                    'Bucket': S3.s3_bucket_name,
                    'Key': key,
                    'ResponseContentDisposition': 'attachment; filename=' + filename
                }
            )

            r_array['result'] = 'success'
            r_array['msg'] = 'Request Success'
            r_array['url'] = url
        except Exception as e:
            r_array['result'] = 'failed'
            r_array['msg'] = 'Request Failed'
            r_array['url'] = ''
        finally:
            return json.dumps(r_array)


    def getfilecontent(self, file_name):
        try:
            with open(file_name, "rb") as image_file:
                encoded_byte = base64.b64encode(image_file.read())
                encoded_string = encoded_byte.decode()
                return encoded_string
        except Exception as e:
            return "Not Found"

    def _get_file_content(self, file_name):
        content_object = self.resource.Object(self.bucket_name, file_name)
        file_content = content_object.get()['Body'].read().decode('utf-8')        
        return file_content

    def copy(self,key,targetKey):
        """copys the object in key to targetKey, 
        needs to be in the same bucket for now
        Argus:
            Key:string, from object key
            targetKey:string, to object key
        
        Returns:
            bool or exception
        """
        try:
            self.client.copy_object(
                Bucket=self.bucket_name,
                CopySource= {
                    'Bucket':self.bucket_name,
                    'Key':key
                },
                Key = targetKey
            )
            return True
        except Exception:
            raise