import os
from collections import namedtuple
from itertools import repeat
import pymysql
import datetime
import decimal
import warnings
import re
warnings.filterwarnings("error")    #use db warnings as errors

""" https://pypi.python.org/pypi/simplemysql """

class SimpleMysql:
    conn = None
    cur = None
    conf = None
    error = None

    def __init__(self, ** kwargs):
            
        self.conf = kwargs
        self.conf["keep_alive"] = kwargs.get("keep_alive", False)
        self.conf["charset"] = kwargs.get("charset", "utf8")

        dir_path = os.path.dirname(os.path.realpath(__file__))
        file_path = dir_path+'/../local/gvars.py'
        
        #if os.path.isfile(file_path): 
        if False: 
            from ..local.gvars import (db_host,db_user,db_passwd,db_name)
            self.conf["host"] = db_host
            self.conf["db"] = db_name
            self.conf["user"] = db_user
            self.conf["passwd"] = db_passwd
        else:
            self.conf["host"] = os.environ['db_host']
            self.conf["db"] = os.environ['db_name']
            self.conf["user"] = os.environ['db_user']
            self.conf["passwd"] = os.environ['db_password']
        
        self.conf["port"] = kwargs.get("port", 3306)
        self.conf["autocommit"] = kwargs.get("autocommit", False)
        self.conf["ssl"] = kwargs.get("ssl", False)
                
        self.connect()

    def connect(self):
        """Connect to the mysql server"""

        try:
            if not self.conf["ssl"]:
                self.conn = pymysql.connect(db=self.conf['db'], host=self.conf['host'],
                                            port=self.conf['port'], user=self.conf['user'],
                                            passwd=self.conf['passwd'],
                                            charset=self.conf['charset'])
            else:
                self.conn = pymysql.connect(db=self.conf['db'], host=self.conf['host'],
                                            port=self.conf['port'], user=self.conf['user'],
                                            passwd=self.conf['passwd'],
                                            ssl=self.conf['ssl'],
                                            charset=self.conf['charset'])
            self.cur = self.conn.cursor()
            self.conn.autocommit(self.conf["autocommit"])
        except:
            print ("MySQL connection failed")
            raise


    def getOne(self, table=None, fields='*', where=None, order=None, limit=(0, 1)):
        """Get a single result

			table = (str) table_name
			fields = (field1, field2 ...) list of fields to select
			where = ("parameterizedstatement", [parameters])
					eg: ("id=%s and name=%s", [1, "test"])
			order = [field, ASC|DESC]
			limit = [limit1, limit2]
		"""

        group = None
        
        cur = self._select(table, fields, where, order, group, limit)
        result = cur.fetchone()

        row = None
        if result:
            Row = namedtuple("Row", [f[0] for f in cur.description])
            row = Row(*result)

        #return row
        return self._format_row(row)


    def getAll(self, table=None, fields='*', where=None, order=None, group=None, limit=None, format_result=True, having=None):
        """Get all results

			table = (str) table_name
			fields = (field1, field2 ...) list of fields to select
			where = ("parameterizedstatement", [parameters])
					eg: ("id=%s and name=%s", [1, "test"])
			order = [field, ASC|DESC]
			limit = [limit1, limit2]
		"""
        if format_result:
            cur = self._select(table=table, fields=fields, where=where, order=order, group=group, limit=limit, is_count=True, having=having)
            result_count = cur.fetchall()

            rows_count = None
            if result_count:
                Row = namedtuple("Row", [f[0] for f in cur.description])
                rows_count = [Row(*r) for r in result_count]
            
        #(table=None, fields=(), where=None, order=None, group=None, limit=None, is_count=None, having=None)
        cur = self._select(table=table, fields=fields, where=where, order=order, group=group, limit=limit, having=having)
        result = cur.fetchall()
        rows = None
        if result:
            Row = namedtuple("Row", [f[0] for f in cur.description])
            rows = [Row(*r) for r in result]

        if format_result:
            if rows_count:
                total_records = self._format_row(rows_count[0])['TOTAL_RECORDS']
            else:
                total_records = 0
            return {"total_records":total_records,"rows":self._format_result(rows)}
        else:
            return self._format_result(rows)
        #return rows
        #return self._format_result(rows)
        
    def customQuery(self, query_str, params=None):
        
        cur = self.query(query_str,params)
        
        result = cur.fetchall()
        rows = None
        if result:
            Row = namedtuple("Row", [f[0] for f in cur.description])
            rows = [Row(*r) for r in result]

        return self._format_result(rows)

    def lastId(self):
        """Get the last insert id"""
        return self.cur.lastrowid

    def lastQuery(self):
        """Get the last executed query"""
        try:
            return self.cur.statement
        except AttributeError:
            return self.cur._last_executed

    def leftJoin(self, tables=(), fields=(), join_fields=(), where=None, order=None, limit=None, format_result=True):
        """Run an inner left join query

			tables = (table1, table2)
			fields = ([fields from table1], [fields from table 2])  # fields to select
			join_fields = (field1, field2)  # fields to join. field1 belongs to table1 and field2 belongs to table 2
			where = ("parameterizedstatement", [parameters])
					eg: ("id=%s and name=%s", [1, "test"])
			order = [field, ASC|DESC]
			limit = [limit1, limit2]
		"""
        if format_result:        
            cur = self._select_join(tables, fields, join_fields, where, order, limit, True)
            result_count = cur.fetchall()

            rows_count = None
            if result_count:
                Row = namedtuple("Row", [f[0] for f in cur.description])
                rows_count = [Row(*r) for r in result_count]
                
            if rows_count:
                total_records = self._format_row(rows_count[0])['TOTAL_RECORDS']
            else:
                total_records = 0

        cur = self._select_join(tables, fields, join_fields, where, order, limit)
        
        try:
            result = cur.fetchall()

            rows = None
            if result:
                Row = namedtuple("Row", [f[0] for f in cur.description])
                rows = [Row(*r) for r in result]
            if format_result:
                return {"total_records":total_records,"rows":self._format_result(rows)}
            else:
                return self._format_result(rows)
        except Exception as e:
            return format(e)
        
    def execQuery(self, tables=[], fields=[], join_fields=(), where=None, order=None, limit=None):
        
        #print(fields)
        newfields = []
        
        for i in range(len(tables)):
            #print(i, tables[i])
            #print(tables[i])
            #print(tables[i][0])
            #print("--------------------")
            #print(fields[i])
            #print("--------------------")
            #print("--------------------")
            newfields = [tables[i][0] + "." + f for f in fields[i]]
            print(newfields)
            print("--------------------")
            
        print(newfields)
        
        pass
        
    def leftJoinOne(self, tables=(), fields=(), join_fields=(), where=None, order=None, limit=None):
        """Run an inner left join query

			tables = (table1, table2)
			fields = ([fields from table1], [fields from table 2])  # fields to select
			join_fields = (field1, field2)  # fields to join. field1 belongs to table1 and field2 belongs to table 2
			where = ("parameterizedstatement", [parameters])
					eg: ("id=%s and name=%s", [1, "test"])
			order = [field, ASC|DESC]
			limit = [limit1, limit2]
		"""
               
                
        cur = self._select_join(tables, fields, join_fields, where, order, limit)
        
        try:
            result = cur.fetchone()
            row = None
            if result:
                Row = namedtuple("Row", [f[0] for f in cur.description])
                row = Row(*result)

            #return row
            return self._format_row(row)
        except Exception as e:
            return format(e)

    def insert(self, table, data):
        """Insert a record"""

        query = self._serialize_insert(data)

        sql = "INSERT INTO %s (%s) VALUES(%s)" % (table, query[0], query[1])

        #return self.query(sql, data.values()).rowcount
        self.query(sql, list(data.values()))
        return self.commit()  # Added by Mubashir Ali

    def insert_ignore(self, table, data):
        query = self._serialize_insert(data)
        sql = "INSERT IGNORE INTO %s (%s) VALUES(%s)" % (table, query[0], query[1])
        self.query(sql, list(data.values()))
        return self.commit()

    def insertBatch(self, table, data):
        """Insert multiple record"""

        query = self._serialize_batch_insert(data)
        sql = "INSERT INTO %s (%s) VALUES %s" % (table, query[0], query[1])
        flattened_values = [v for sublist in data for k, v in sublist.iteritems()]
        return self.query(sql, flattened_values).rowcount

    def update(self, table, data, where=None):
        """Insert a record"""

        query = self._serialize_update(data)
        
        sql = "UPDATE %s SET %s" % (table, query)

        if where and len(where) > 0:
            sql += " WHERE %s" % where[0]

        #return self.query(sql, data.values() + where[1] if where and len(where) > 1 else data.values()).rowcount
        res = self.query(sql, where[1])

        if res:
            return self.commit()    #Added by Mubashir Ali
        return False


    def insertOrUpdate(self, table, data, keys):
        insert_data = data.copy()

        data = {k: data[k] for k in data if k not in keys}

        insert = self._serialize_insert(insert_data)

        update = self._serialize_update(data)

        sql = "INSERT INTO %s (%s) VALUES(%s) ON DUPLICATE KEY UPDATE %s" % (table, insert[0], insert[1], update)

        return self.query(sql, insert_data.values() + data.values()).rowcount

    def delete(self, table, where=None):
        """Delete rows based on a where condition"""

        sql = "DELETE FROM %s" % table

        if where and len(where) > 0:
            sql += " WHERE %s" % where[0]

        res = self.query(sql, where[1] if where and len(where) > 1 else None).rowcount
        if res:
            return self.commit()    #Added by Mubashir Ali
        return False


    def query(self, sql, params=None):
        """Run a raw query"""
        print("\n\n")
        print("Query: ",sql)
        print("Parameters: ",params)
        # check if connection is alive. if not, reconnect
        try:
            self.cur.execute(sql, params) 
        #except OperationalError as e:
        #    # mysql timed out. reconnect and retry once
        #    if e[0] == 2006:
        #        self.connect()
        #        self.cur.execute(sql, params)
        #    else:
        #        raise
        #except MySQLError as e:
        #    print('Got error {!r}, errno is {}'.format(e, e.args[0]))
        except pymysql.InternalError as error:
            code, message = error.args
            self.error = 'Error Code: '+str(code)+", Details: "+message
            print({"error 1":self.error})
            return {"error 1":self.error}
        except Exception as e:
            self.error = format(e)
            print({"error 2":self.error})
            return {"error 2":self.error}
        except RuntimeWarning as w:
            print({"warning":format(w)})
            return {"warning":format(w)}
        except:
            self.error = 'Query failed'
            print({"error 3":self.error})
            return {"error 3":self.error}
            raise
        
        self.error = None

        return self.cur

    def commit(self):
        """Commit a transaction (transactional engines like InnoDB require this)"""
        if self.error:
            raise Exception(self.error)
        
        try:
            self.conn.commit()
            return True
        except RuntimeWarning as w:
            return {"warning":format(w)}
        except Exception as e:
            return {"error commit":format(e)}

    def is_open(self):
        """Check if the connection is open"""
        return self.conn.open

    def end(self):
        """Kill the connection"""
        self.cur.close()
        self.conn.close()

    # ===

    def _serialize_insert(self, data):
        """Format insert dict values into strings"""
        keys = ",".join(data.keys())
        vals = ",".join(["%s" if k else Null for k in data])

        return [keys, vals]

    def _serialize_batch_insert(self, data):
        """Format insert dict values into strings"""
        keys = ",".join(data[0].keys())
        v = "(%s)" % ",".join(tuple("%s".rstrip(',') for v in range(len(data[0]))))
        l = ','.join(list(repeat(v, len(data))))
        return [keys, l]

    def _serialize_update(self, data):
        """Format update dict values into string"""
        ####return "=%s,".join(data.keys()) + "=%s"  ### Original
        #return ','.join('{}=\'{}\''.format(key, re.escape(val)) for key, val in data.items())  #Updated by Mubashir Ali
        return ','.join('{}=\'{}\''.format(key, val) for key, val in data.items())  #Updated by Mubashir Ali
    
    def html_escape(self, text):
        text = text.replace("\"", "&quot;")
        #text = text.replace(">", "&gt;")
        #text = text.replace("<", "&lt;")
        #text = text.replace("&", "&amp;")
        return text

        """Produce entities within text."""
        html_escape_table = {
            "&": "&amp;",
            '"': "&quot;",
            "'": "&apos;",
            ">": "&gt;",
            "<": "&lt;",
        }
        return "".join(html_escape_table.get(c,c) for c in text)


    def _select(self, table=None, fields=(), where=None, order=None, group=None, limit=None, is_count=None, having=None):
        """Run a select query"""

        sql = "SELECT %s FROM `%s`" % (",".join(fields), table)
        part_sql = ""

        # where conditions
        if where and len(where) > 0:
            part_sql += " WHERE %s" % where[0]

        # order
        if order:
            part_sql += " ORDER BY %s" % order[0]

            if len(order) > 1:
                part_sql += " %s" % order[1]

        # group
        if group:
            part_sql += " GROUP BY %s" % group[0]

            if len(group) > 1:
                part_sql += " %s" % group[1]

        # having
        if having:
            part_sql += " HAVING %s " % having
                
        sql += part_sql
                
        if is_count:
            count_sql = "SELECT COUNT(*) AS TOTAL_RECORDS FROM `%s`" % (table)
            count_sql += part_sql
            return self.query(count_sql, where[1] if where and len(where) > 1 else None)

        # limit
        if limit:
            sql += " LIMIT %s" % limit[0]

            if len(limit) > 1:
                sql += ", %s" % limit[1]

        return self.query(sql, where[1] if where and len(where) > 1 else None)

    def _select_join(self, tables=(), fields=(), join_fields=(), where=None, order=None, limit=None, is_count=None):
        """Run an inner left join query"""

        fields = [tables[0] + "." + f for f in fields[0]] + \
            [tables[1] + "." + f for f in fields[1]]

        sql = "SELECT %s FROM %s LEFT JOIN %s ON (%s = %s)" % \
            (",".join(fields),
             tables[0],
             tables[1],
             tables[0] + "." + join_fields[0], \
             tables[1] + "." + join_fields[1]
             )

        count_sql = "SELECT %s FROM %s LEFT JOIN %s ON (%s = %s)" % \
            ("COUNT(*) AS TOTAL_RECORDS",
             tables[0],
             tables[1],
             tables[0] + "." + join_fields[0], \
             tables[1] + "." + join_fields[1]
             )
             
        #print(sql)

        # where conditions
        if where and len(where) > 0:
            sql += " WHERE %s" % where[0]
            count_sql += " WHERE %s" % where[0]

        # order
        if order:
            sql += " ORDER BY %s" % order[0]

            if len(order) > 1:
                sql += " " + order[1]
                
                
        if is_count:
            return self.query(count_sql, where[1] if where and len(where) > 1 else None)

        # limit
        if limit:
            sql += " LIMIT %s" % limit[0]

            if len(limit) > 1:
                sql += ", %s" % limit[1]

        return self.query(sql, where[1] if where and len(where) > 1 else None)
            
    # Added by Mubashir Ali
    def _format_result(self, res):
        if res is not None:
            r_array = []
            for row in res:
                #tmp_row = {}
                #for key in row._fields:
                #    tmp_row[key] = eval("row." + key)
                #r_array.append(tmp_row)
                r_array.append(self._format_row(row))
            return r_array
        else:
            return False
    
    # Added by Mubashir Ali
    def _format_row(self, row):
        if row is not None:
            tmp_row = {}
            for key in row._fields:
                value = eval("row." + key)
                
                #print( type(value) )

                if not value:
                    tmp_row[key] = ""
                if type(value) is datetime.datetime:
                    #tmp_row[key] = value.strftime('%d/%m/%Y %H:%M:%S')
                    tmp_row[key] = value.strftime('%Y-%m-%d %H:%M:%S')
                elif type(value) is datetime.date:
                    #tmp_row[key] = value.strftime('%d/%m/%Y')
                    tmp_row[key] = value.strftime('%Y-%m-%d')
                elif type(value) is datetime.timedelta:
                    tmp_row[key] = ':'.join(str(value).split(':')[:3])
                elif type(value) is decimal.Decimal:
                    tmp_row[key] = str(value)
                elif type(value) is None:
                    tmp_row[key] = ""
                elif value is None:
                    tmp_row[key] = ""
                else:
                    tmp_row[key] = value
            return tmp_row
        else:
            return False

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        self.end()
